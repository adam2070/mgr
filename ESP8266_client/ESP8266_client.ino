#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <SoftwareSerial.h>
#include <PubSubClient.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>

#define BAUDRATE 9600

#define RX_ARDUINO D7
#define TX_ARDUINO D8
SoftwareSerial arduinoSerial(RX_ARDUINO, TX_ARDUINO);

const char* SSID = "UPC3989988";
const char* PASSWORD = "secret";

WiFiClient wifi_client;

#define MQTT_SERVER "192.168.0.47"
//#define MQTT_SERVER "192.168.0.115"
#define MQTT_PORT 1883
#define TOPIC "measurements"
PubSubClient mqtt_client(wifi_client);

String last_message;
String co2_state; 

const long utcOffsetInSeconds = 3600;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

unsigned long get_time(){
  timeClient.update();
  return timeClient.getEpochTime();
}

void setup() { 
  pinMode(D1, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT); 
  //digitalWrite(D1, HIGH);
  Serial.begin(BAUDRATE);
  arduinoSerial.begin(BAUDRATE);
  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  mqtt_client.setServer(MQTT_SERVER, MQTT_PORT);
  if (mqtt_client.connect("esp8266")) {
    Serial.println("Connected to MQTT!");
  } else {
    Serial.println("Connection to MQTT failed: "/* + mqtt_client.state()*/);
  }
  delay(1000);
}

void publish_message(String message, String parameter_type){
  StaticJsonDocument<200> json_obj;
  String json_str;
  message.trim();
  
  json_obj["timestamp"] = get_time();
  json_obj["parameter"] = message.substring(3,4);
  json_obj["id"] = message.substring(3,4);
  json_obj["value"] = message.substring(4);
  Serial.println(message.substring(3,4));
  serializeJson(json_obj, json_str);
  if (mqtt_client.publish(TOPIC, json_str.c_str()))
    Serial.print("sending message");
  else
    Serial.print("sending failed");
}

void loop() {

    if (Serial.available()){
        last_message = Serial.readStringUntil('#');
        Serial.println(last_message);
        digitalWrite(LED_BUILTIN, LOW);
        if (last_message.indexOf("CO2") >= 0)
          publish_message(last_message, "CO2");
        else if (last_message.indexOf("PRS") >= 0)
          publish_message(last_message, "pressure");
    }
}
