#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <SoftwareSerial.h>
#include <PubSubClient.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>

#define BAUDRATE 9600

#define HTTP_REST_PORT 8080
ESP8266WebServer server(HTTP_REST_PORT);

#define RX_ARDUINO D8
#define TX_ARDUINO D7
SoftwareSerial arduinoSerial(RX_ARDUINO, TX_ARDUINO);

const char* SSID = "UPC8099732";
const char* PASSWORD = "s5fwnusmbknW";

enum StatusCode {
  BR_OK, BR_FAIL, BR_CONNECTION_ERROR, BR_MQTT_ERROR
};

char *ErrorCodeToString(StatusCode err){
  switch (err){
    BR_OK: return "OK";
    BR_FAIL: return "FAIL";
    BR_CONNECTION_ERROR: return "CONNECTION_ERROR";
    BR_MQTT_ERROR: return "MQTT_ERROR";
    default: return "UNDEFINED";
  }
}

//const char* SSID = "inz115";
//const char* PASSWORD = "HS-GC-FPD";
WiFiClient wifi_client;

#define MQTT_SERVER "192.168.0.47"
//#define MQTT_SERVER "192.168.0.115"
#define MQTT_PORT 1883
#define TOPIC "measurements"
PubSubClient mqtt_client(wifi_client);

String last_message;
String co2_state; 

const long utcOffsetInSeconds = 3600;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

unsigned long get_time(){
  timeClient.update();
  return timeClient.getEpochTime();
}

void getSensorState() {
  String sensor_id = server.arg("sensor");
  StatusCode status;
  
  status = send_request_to_arduino(sensor_id);
  server.send(200, "text/plain", sensor_id + " " + ErrorCodeToString(status));
}
/* TODO
void send_request_to_arduino_non_blocking(String sensor_id){
  String last_message;
  digitalWrite(LED_BUILTIN, LOW);
  arduinoSerial.println("get " + sensor_id + "#");
} */

StatusCode send_request_to_arduino(String sensor_id){  
  String last_message;
  int tries = 0;
  StatusCode status = BR_OK;
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("Sending to Arduino: get " + sensor_id + "#");
  arduinoSerial.println("get " + sensor_id + "#");
  
  delay(100);
  while (Serial.available() > 0) {
    last_message = Serial.readStringUntil('#');
    Serial.println("Message received: " + last_message);
    delay(10);
    
    if (last_message.indexOf("MSRA") >= 0) {
      status = publish_message(last_message);
    }
    digitalWrite(LED_BUILTIN, HIGH);
  }
  return status;
}

void getHelloWorld() {
    server.send(200, "text/json", "{\"name\": \"Hello world\"}");
}


void restServerRouting() {
    server.on("/", HTTP_GET, []() {
        server.send(200, F("text/html"),
            F("Welcome to the REST Web Server"));
    });
    server.on(F("/helloWorld"), HTTP_GET, getHelloWorld);
    server.on(F("/sensor_state"), HTTP_GET, getSensorState);
}

void setup() { 
  pinMode(D1, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT); 
  Serial.begin(BAUDRATE);
  arduinoSerial.begin(BAUDRATE);
  
  WiFi.begin(SSID, PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  restServerRouting();
  server.begin();
  timeClient.begin();
  mqtt_client.setServer(MQTT_SERVER, MQTT_PORT);
  mqtt_client.setKeepAlive(3600); 
  digitalWrite(LED_BUILTIN, HIGH);

  while(!Serial);
  while(!arduinoSerial);
  Serial.println("Setup done");
}


StatusCode publish_message(String message){
  StaticJsonDocument<200> json_obj;
  String json_str;
  message.trim();
  
  json_obj["timestamp"] = get_time();
  json_obj["parameter"] = message.substring(3,5);
  json_obj["id"] = message.substring(5,6);
  json_obj["value"] = message.substring(6);
  serializeJson(json_obj, json_str);
  if (mqtt_client.connect("esp8266")) {
    Serial.println("Connected to MQTT!");
  } else {
    Serial.println("Connection to MQTT failed. ");
    return BR_MQTT_ERROR;
  }
  if (mqtt_client.publish(TOPIC, json_str.c_str()))
    return BR_OK;
  else
    return BR_CONNECTION_ERROR;
}

void loop() {
  server.handleClient();
}
