#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, time, base64, json, os, subprocess
from paho.mqtt import client as mqtt_client
from paho.mqtt import publish

# arg 1: topic
# arg 2: filename

SERVER_IP='192.168.0.47'
#SERVER_IP='192.168.0.115'
PORT=1883
AUTH={'username': "raspberry", 'password':"password"}
ID='raspberry'

topic = sys.argv[1]

def get_gasflow(slave_id):
	value = 0
	for i in range(20):
		completed_process = subprocess.run(['/home/pi/mgr/Raspberry/scripts/red.sh', "get_gas_flow", str(slave_id)], capture_output=True)
		#print(completed_process.stdout)
		value += float(completed_process.stdout)
		time.sleep(5)

	value /= 20
	#print(value)
	return value

def run():
	slave_id=2
	if topic == "photos":
		filename = sys.argv[2]
		file = open(filename, "rb")
		image = file.read()
		byte_image = base64.b64encode(bytearray(image)).decode()
		publish.single(topic, payload=json.dumps({'date': os.path.basename(filename), 'image': byte_image, 'type': 'photo'}),
			qos=1, hostname=SERVER_IP,	port=PORT, client_id=ID, auth=AUTH)
	elif topic == "redy":
		gasflow = get_gasflow(slave_id)
		publish.single(topic, payload=json.dumps({'timestamp': time.time(), 'id': slave_id, 'parameter': 'gas flow', 'value': gasflow}),
			qos=1, hostname=SERVER_IP,	port=PORT, client_id=ID, auth=AUTH)

if __name__ == '__main__':
    run()
