#!/bin/bash

# $1 - job
# $minutes_n - interval (minutes)
# $hours_n - interval (hours)

set -o noglob

minutes_n=$2
hours_n=$3

if [[ $minutes_n -gt 0 ]] && [[ $minutes_n -lt 60 ]]; then
	minutes="*/$minutes_n"
elif
	[ $minutes_n == 0 ] || [ -z $minutes_n ] ; then
	minutes="0"
fi
if [[ $hours_n -gt 0 ]] && [[ $hours_n -lt 60 ]]; then
	hours="*\/$hours_n"
elif [[ $hours_n == 0 ]] || [ -z $hours_n ]; then
	hours="*"
fi
if [ $1 = "camera" ]; then
	job="$minutes $hours * * * /home/pi/mgr/Raspberry/scripts/camera.sh >> /home/pi/logs/camera.log"
	prev_job=camera.sh
elif [ $1 = "gasflow" ]; then
	job="$minutes $hours * * * /home/pi/mgr/Raspberry/scripts/send_gasflow.sh >> /home/pi/logs/gasflow.log"
	prev_job=get_gasflow.sh
#elif [ $1 = "crontab_camera" ]; then
#	job="$minutes_n /home/pi/mgr/Raspberry/scripts/camera.sh >> /home/pi/logs/camera.log"
#	prev_job=camera.sh
#elif [ $1 = "gasflow_camera" ]; then
#	job="$minutes_n /home/pi/mgr/Raspberry/scripts/send_gasflow.sh >> /home/pi/logs/gasflow.log"
#	prev_job=get_gasflow.sh
fi



crontab -l | grep -v "$prev_job" | crontab -
(crontab -l ; echo "$job") | crontab -
set +o noglob
