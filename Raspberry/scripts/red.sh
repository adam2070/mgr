#!/bin/bash

# $1 - option name
# $2 - slave id
# $3 - value

#avaliable options:
# get_gas
# get_temp
# get_total_gas

tty=/dev/ttyUSB0
baud=9600
stop_bits=2
parity_bits=n
slave_id=$2
LOCAL_PATH=/home/pi/.local/bin
REGISTER_PATH=/home/pi/mgr/Raspberry/scripts

option=$1
if [ -z "$3" ] ; then 
    #get
    set=false
    value=""
else
    #set 
    set=true
    value='='$3
fi
#echo $value
#echo "$LOCAL_PATH/modbus $tty -r registers.modbus -s $slave_id -b=$baud -p=$stop_bits -P=$parity_bits $option$value"
#$LOCAL_PATH/modbus $tty -r registers.modbus -s $slave_id -b=$baud -p=$stop_bits -P=$parity_bits $option$value
output=`$LOCAL_PATH/modbus $tty -r $REGISTER_PATH/registers.modbus -s $slave_id -b=$baud -p=$stop_bits -P=$parity_bits $option$value | tail -n 1 | cut -d " " -f 2`
if [ $set == false ] ; then
    echo $output
fi
