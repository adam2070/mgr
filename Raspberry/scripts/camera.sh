#!/bin/bash
#Takes photo every 5 minutes and sends it to MQTT queue

SCRIPTS_DIR=/home/pi/mgr/Raspberry/scripts
DATE=$(date +"%Y-%m-%d_%H%M%S")
FILENAME=/home/pi/camera/$DATE.jpg
raspistill -vf -hf -o $FILENAME
chown pi $FILENAME
$SCRIPTS_DIR/send_to_mqtt.py "photos" $FILENAME &>> /home/pi/logs/camera.log
rm -f $FILENAME
