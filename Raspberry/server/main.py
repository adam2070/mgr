#!/usr/bin/env python3
import flask as fl
import os, subprocess

app = fl.Flask(__name__, static_url_path='/static')

def check_filename(str):
	return ("/" not in str and "\\" not in str)

@app.route('/')
def index():
	name = fl.request.args.get("name", default="World")
	return f'Hello {fl.escape(name)}'

@app.route('/photo_interval')
def change_camera_interval():
	minutes = fl.request.args.get("minutes")
	hours = fl.request.args.get("hours")
	completed_process = subprocess.run(['/home/pi/mgr/Raspberry/scripts/modify_crontab_job.sh',"camera", minutes, hours], capture_output=True)
	return completed_process.stdout

@app.route('/redy_get')
def redy_get():
	option = fl.request.args.get("option")
	slave_id = fl.request.args.get("id", default="2")
	completed_process = subprocess.run(['/home/pi/mgr/Raspberry/scripts/red.sh', option, slave_id], capture_output=True)
	return completed_process.stdout

@app.route('/redy_set')
def redy_set():
	option = fl.request.args.get("option")
	value = fl.request.args.get("value", default="0")
	slave_id = fl.request.args.get("id", default="1")
	redy_set_script(option, value, slave_id)
	return "Done"

def redy_set_script(option, value, slave_id):
	subprocess.run(['/home/pi/mgr/Raspberry/scripts/red.sh', option, slave_id, value], capture_output=True)

@app.route('/camera')
def camera():
	os.system("raspistill -o images/image.jpg -w 1920 -h 1080")
	return fl.send_from_directory("images", "image.jpg")

if __name__ == '__main__':
	app.run(debug=True, port=80, host='0.0.0.0')