import Scheduler


class ControlHandler(object):
    def __init__(self, database, mqtt):
        self.db = database
        self.scheduler = Scheduler.Scheduler(database.sensors)
        self.mqtt = mqtt
        self.jobs = []

    def is_system_on(self):
        f = open("/var/control/on-off", "r")
        state = f.read(1)
        f.close()
        if state == "0":
            return True
        else:
            return False

    def write_state(self, on):
        f = open("/var/control/on-off", "w")
        if on:
            f.write("0")
        else:
            f.write("1")
        f.close()

    def setup_app(self):
        if self.is_system_on():
            print("Setup app")
            self.scheduler.start_scheduler()
            self.mqtt.subscribe("photos")
            self.mqtt.subscribe("measurements")
            # self.mqtt.subscribe("redy")

    def stop_app(self):
        self.mqtt.unsubscribe_all()
        self.scheduler.shutdown()

    def on_off_button_handler(self):
        if self.is_system_on():
            print("Stopping app")
            self.write_state(False)
            self.stop_app()
        else:
            print("Starting app")
            self.write_state(True)
            self.setup_app()

    def reset_scheduler(self):
        self.scheduler.reset_scheduler()
