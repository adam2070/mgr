from apscheduler.schedulers.background import BackgroundScheduler
from bson.objectid import ObjectId
import requests


class Scheduler(object):
	def __init__(self, sensor_collection):
		self.sensors = sensor_collection
		self.create_scheduler()
		self.jobs = []

	def reset_scheduler(self):
		self.scheduler.shutdown()
		del self.scheduler
		self.start_scheduler()

	def start_scheduler(self):
		self.create_scheduler()
		self.scheduler.start()
		self.add_jobs()

	def add_jobs(self):
		for sensor_group in find_jobs_at_same_time(self.sensors):
			self.create_bulk_job(sensor_group)
		
		for job in self.scheduler.get_jobs():
			print(job.args)
		# for sensor in self.db.sensors.find():
		#    print("Job ", sensor)
		# self.add_job(sensor)

	def create_bulk_job(self, sensor_group):
		bulk_sensor_string = ""
		for sensor_obj_id in sensor_group["uniqueIds"]:
			sensor = self.sensors.find_one({"_id": ObjectId(sensor_obj_id)})
			bulk_sensor_string += sensor["sensor_id"]

		sensor["sensor_id"] = bulk_sensor_string
		self.add_job(sensor)

	def add_job(self, sensor):
		freq = sensor["częstotliwość"]
		sensor_id = sensor["sensor_id"]
		ip = sensor["arduino_ip"]
		nazwa = sensor["name"]
		self.scheduler.add_job(
			get_sensor_data,
			"interval",
			args=[sensor_id, ip],
			minutes=int(freq),
			id=nazwa,
		)

	def shutdown(self):
		self.scheduler.shutdown()

	def create_scheduler(self):
		self.scheduler = BackgroundScheduler(job_defaults={"max_instances": 100})



def get_sensor_data(sensor, ip):
	try:
		url = "http://" + ip + ":8080/sensor_state"
		requests.get(url, params={"sensor": sensor})
	except OSError as error:
		print(error, "Arduino", ip, "is offline")


def find_jobs_at_same_time(sensors):
	query = [
		{
			"$group": {
				"_id": {
					"arduino_id": "$arduino_id",
					"arduino_ip": "$arduino_ip",
					"częstotliwość": "$częstotliwość",
				},
				"uniqueIds": {"$addToSet": "$_id"},
			},
		}
	]

	return sensors.aggregate(query)
