from flask_admin.contrib.pymongo import ModelView
from wtforms import form, fields, validators


class MeasurementForm(form.Form):
    timestamp = fields.StringField("timestamp")
    parameter = fields.StringField("parameter")
    id = fields.StringField("id")
    value = fields.FloatField("value")


class MeasurementView(ModelView):
    column_list = ("timestamp", "parameter", "id", "value")
    form = MeasurementForm
    can_export = True
    can_create = False
    can_edit = False


class SensorForm(form.Form):
    # nazwa pola jest ważna, a nie label!
    arduino_id = fields.StringField("Arduino ID")
    arduino_ip = fields.StringField("IP", validators=[validators.IPAddress()])
    sensor_id = fields.StringField("Port")
    unit = fields.StringField("Jednostka")
    a = fields.FloatField("a")
    b = fields.FloatField("b")
    częstotliwość = fields.StringField("częstotliwość (minuty)")
    name = fields.StringField("nazwa")


class SensorView(ModelView):
    column_list = (
        "arduino_id",
        "arduino_ip",
        "sensor_id",
        "unit",
        "a",
        "b",
        "częstotliwość",
        "name",
    )
    form = SensorForm


class AlarmForm(form.Form):
    # def __init__(self, list=None, **kwds):
    #    self.sensor_list = list
    #    super(AlarmForm, self).__init__(**kwds)

    sensor = fields.SelectField("sensor")
    trigger = fields.StringField("trigger")
    on_off = fields.BooleanField("on_off")


class AlarmView(ModelView):
    """
    def __init__(self, coll, name, list=None, **kwds):
        #self.form = AlarmForm()
        AlarmForm.sensor.choices = [(24352345, 'xD'), (3244, 'XDD')]
        self.form.sensor.choices = list
        super(AlarmView, self).__init__(coll, name, **kwds)

        #self.list = list
        print("xD")
        #form.sensor.choices = [(24352345, 'xD'), (3244, 'XDD')]
        print(self.form.sensor)
        print(self.form.sensor.choices)
    """

    column_list = (
        "sensor",
        "trigger",
        "on_off",
    )

    form = AlarmForm


# form.sensor.choices = [(24352345, 'xD'), (3244, 'XDD')]

# def set_sensor_choices(self, list):
#     self.form.sensor.choices = list
