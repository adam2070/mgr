from datetime import datetime
import sys, json
import AlarmHandler


class MeasurementHandler(object):
    def __init__(self, database):
        self.db = database
        self.alarmer = AlarmHandler.AlarmHandler("s160241@student.pg.gda.pl")

    def compute_measurement(self, payload_json):
        print(payload_json, file=sys.stdout)
        try:
            sensor = self.db.sensors.find({"arduino_id": payload_json["id"]})[0]
            value = float(sensor["a"]) * int(payload_json["value"]) + float(sensor["b"])
            return value
        except Exception as e:
            print("Error: no such sensor")
            return e

    def handle_measurement(self, measurement_json):
        time = int(measurement_json["timestamp"])
        measurement_json["timestamp"] = datetime.utcfromtimestamp(time).strftime(
            "%Y-%m-%d %H:%M:%S"
        )
        self.check_for_alarms(measurement_json)

        if not self.check_if_record_exists(measurement_json):
            try:
                self.db.measurements.update_one(
                    measurement_json, {"$set": measurement_json}, upsert=True
                )
            except Exception as e:
                print("Error adding measurement ", measurement_json)

    def check_if_record_exists(self, measurement_json):
        timestamp = measurement_json["timestamp"]
        id: string = measurement_json["id"]
        parameter = measurement_json["parameter"]

        return (
            self.db.measurements.count_documents(
                {"id": id, "timestamp": timestamp, "parameter": parameter}
            )
            > 0
        )

    def handle_measurement_from_arduino(self, measurement_json):
        measurement_value = self.compute_measurement(measurement_json)
        measurement_json["value"] = measurement_value
        self.handle_measurement(measurement_json)

    def check_for_alarms(self, measurement):
        for alarm in self.db.alarms.find():
            self.check_single_alarm(alarm, measurement)

    def check_single_alarm(self, alarm, measurement):
        sensor = (
            alarm["sensor"].replace("'", '"').replace("ObjectId(", "").replace(")", "")
        )
        sensor = json.loads(sensor)
        sensor_id = sensor["sensor_id"]
        name = sensor["name"]
        arduino_id = sensor["arduino_id"]

        if (
            sensor_id == measurement["parameter"]
            and arduino_id == measurement["id"]
            and alarm["on_off"]
        ):
            trigger = alarm["trigger"]
            limit = alarm["limit"]
            print("sensor: ", sensor, type(sensor), file=sys.stdout)

            print(measurement["value"], file=sys.stdout)
            if trigger == ">":
                if measurement["value"] <= limit:
                    return
            elif trigger == "<":
                if measurement["value"] >= limit:
                    return

            message = (
                "Alarm na Arduino "
                + str(arduino_id)
                + ", sensor "
                + str(sensor_id)
                + ": "
                + str(name)
            )
            print(message, file=sys.stdout)
            self.alarmer.send_alarm(message)

    # def clean_duplicates(self):
    #   db.measurements.aggregate([{"$group" : {_id:{timestamp:"$timestamp"}, Dups:{$addToSet:"$_id"}}}]).foreach
