#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import ControlHandler
import MeasurementHandler
from flask_mqtt import Mqtt
import Views
import flask as fl
import os, base64, json, pymongo, sys
from flask_admin import Admin, AdminIndexView, expose
import requests
from flask_admin.contrib.pymongo import ModelView
from wtforms import form, fields

app = fl.Flask(__name__, static_url_path="/static")
app.config["FLASK_ADMIN_SWATCH"] = "cerulean"
admin = Admin(app, name="Sensory", template_mode="bootstrap3")

RASPBERRY_IP = "192.168.0.96"
# RASPBERRY_IP = "raspberrypi"
MONGO_PORT = 27017

app.config["MQTT_BROKER_URL"] = "mosquitto"
app.config["MQTT_BROKER_PORT"] = 1883
app.config["MQTT_USERNAME"] = "serwer"
app.config["MQTT_PASSWORD"] = "password"
app.config["MQTT_KEEPALIVE"] = 3600
app.config["MQTT_CLIENT_ID"] = "serwer"
app.config["SESSION_TYPE"] = "mongodb"

mongo_client = pymongo.MongoClient(host="mongodb", port=MONGO_PORT)
db = mongo_client.database
mqtt = Mqtt(app)
ch = ControlHandler.ControlHandler(db, mqtt)
mh = MeasurementHandler.MeasurementHandler(db)

last_gasflow = 0


def save_photo(path, photo):
    f = open(path, "wb")
    f.write(photo)
    f.close()


@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    print(level, buf)


@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    print("message received on topic", message.topic, file=sys.stdout)
    topic = message.topic
    payload_json = json.loads(message.payload)
    if topic == "photos":
        photo_b64 = payload_json["image"]
        filename = payload_json["date"]
        decoded_photo = base64.b64decode(photo_b64)
        save_photo("/var/photos/" + filename, decoded_photo)
        save_photo("static/most_recent_photo.jpg", decoded_photo)
    elif topic == "measurements":
        mh.handle_measurement_from_arduino(payload_json)


@app.route("/")
def index():
    return "Hello world"


@app.route("/on_off_button/", methods=["POST"])
def on_off_button():
    ch.on_off_button_handler()
    return fl.redirect("/admin")


@app.route("/reset_scheduler_button/", methods=["POST"])
def reset_scheduler_button():
    ch.reset_scheduler()
    return fl.redirect("/admin")


@app.route("/refresh_alarms_button/", methods=["POST"])
def refresh_alarms_button():
    AlarmForm.sensor.choices = get_sensor_list()
    AlarmView.form = AlarmForm

    return fl.redirect("/admin")


@app.route("/change_photo_freq/", methods=["POST"])
def change_photo_freq():
    minutes = fl.request.form["minuty"]
    hours = fl.request.form["godziny"]
    url = "http://" + RASPBERRY_IP + ":80/photo_interval"
    r = requests.get(url, params={"minutes": minutes, "hours": hours})
    return fl.redirect("/admin")


@app.route("/set_gasflow", methods=["POST"])
def set_gasflow():
    value = fl.request.form["value"]
    id = fl.request.form["id"]
    url = "http://" + RASPBERRY_IP + ":80/redy_set"
    r = requests.get(
        url, params={"option": "setpoint_gas_flow", "value": value, "id": id}
    )
    return fl.redirect("/admin")


@app.context_processor
def is_system_on_flask():
    if ch.is_system_on():
        return dict(is_system_on="True")
    else:
        return dict(is_system_on="False")


@app.context_processor
def get_gasflow_context_processor():
    return dict(get_gas_flow=read_gasflow())


@app.route("/get_gasflow", methods=["POST"])
def get_gasflow_button():
    get_gasflow()
    return fl.redirect("/admin")


def get_gasflow():
    # id = fl.request.form['id']
    url = "http://" + RASPBERRY_IP + ":80/redy_get"
    r = requests.get(url, params={"option": "get_gas_flow"})
    f = open("/var/control/last_gasflow", "w")
    f.write(r.text)
    f.close()


def read_gasflow():
    f = open("/var/control/last_gasflow", "r")
    gasflow = f.read()
    f.close()
    return gasflow


def get_sensor_list():
    return [(item, item["name"]) for item in db.sensors.find()]


class AlarmForm(form.Form):
    # def __init__(self, list=None, **kwds):
    #    self.sensor_list = list
    #    super(AlarmForm, self).__init__(**kwds)

    sensor = fields.SelectField("sensor", choices=get_sensor_list())
    trigger = fields.SelectField(
        "trigger", choices=[(">", "większe niż"), ("<", "mniejsze niż")]
    )
    limit = fields.FloatField("limit")
    on_off = fields.BooleanField("on_off")


class AlarmView(ModelView):
    column_list = (
        "sensor",
        "trigger",
        "limit",
        "on_off",
    )

    form = AlarmForm

    def scaffold_form(self):
        form = super(AlarmView, self).scaffold_form()

        form.status.choices = get_sensor_list()

        return form

    def _handle_view(self, name, **kwargs):
        # re-scaffold views every request
        self._refresh_cache()

        return super(AlarmView, self)._handle_view(name, **kwargs)


alarm_view = AlarmView(db.alarms, "Alarmy")

if __name__ == "__main__":
    print("Let's go!")
    admin.add_view(Views.MeasurementView(db.measurements, "Pomiary"))
    admin.add_view(Views.SensorView(db.sensors, "Sensory"))
    admin.add_view(alarm_view)

    app.secret_key = "sdsdfdsfghjdshugfdsdufygj"

    ch.setup_app()
    print(mqtt.client._client_id)
    app.run(debug=False, port=80, host="0.0.0.0", threaded=True, use_reloader=False)
