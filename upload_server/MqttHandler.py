from flask_mqtt import Mqtt


@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print("test")
    if rc == 0:
        print("Connected to MQTT Broker!", file=sys.stdout)
    else:
        print("Failed to connect, return code %d\n", rc, file=sys.stdout)
    mqtt.subscribe("photos")
    mqtt.subscribe("measurements")
    mqtt.subscribe("redy")


def save_photo(path, photo):
    f = open(path, "wb")
    f.write(photo)
    f.close()


@mqtt.on_log()
def handle_logging(client, userdata, level, buf):
    print(level, buf)


@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    print("message received on topic", message.topic, file=sys.stdout)
    topic = message.topic
    payload_json = json.loads(message.payload)
    if topic == "photos":
        photo_b64 = payload_json["image"]
        filename = payload_json["date"]
        decoded_photo = base64.b64decode(photo_b64)
        save_photo("/var/photos/" + filename, decoded_photo)
        save_photo("static/most_recent_photo.jpg", decoded_photo)
    elif topic == "measurements":
        mh.handle_measurement_from_arduino(payload_json)
