#include <SoftwareSerial.h>
#define S3_PIN A3
#define RX_PIN 5
#define TX_PIN 4
#define LED_PIN 52
#define BAUDRATE 9600
#define SAMPLE_SIZE 50
#define N_SENSORS 6

const int id = 1;
const float mV_multiplier = 5.0/1.023;

#define BIT(nr) (1UL << (nr))

SoftwareSerial espSerial(RX_PIN, TX_PIN);
String last_message;
unsigned readouts[N_SENSORS];


void fill_readouts()
{
  unsigned temp_readouts[] = {0, 0, 0, 0, 0, 0, 0, 0};
  
  for (int i = 0; i < SAMPLE_SIZE; i++) {
/* analogRead(pin) - read from analog pin */
    temp_readouts[0] += analogRead(A0);
    temp_readouts[1] += analogRead(A1);
    temp_readouts[2] += analogRead(A2);
    temp_readouts[3] += analogRead(A3);
    temp_readouts[4] += analogRead(A4);
    temp_readouts[5] += analogRead(A5);
    delay(int(10000.0/float(SAMPLE_SIZE)));
  }
  for (int j = 0; j<8; j++) {
    readouts[j] = int(temp_readouts[j]/float(SAMPLE_SIZE));
  }
}



/* Wysyła wiadomość o treści "MSRA<numer pinu analogowego><odczyt z sensora>#"
 * Przetworzenie danych odbywa się po stronie serwera za pomocą parametrów
 * podanych przez użytkownika. 
 */
void send_message(String prefix, int pin)
{
	String message_to_send;
    int measurement = readouts[pin];
	int tries = 0;
  
	message_to_send = prefix + String(id) + String(measurement) + "#";
  Serial.println("Debug: " + message_to_send);
	espSerial.println(message_to_send);
}

void handle_message(String message)
{
  char sensor[8];
  for (int i = 0; i < N_SENSORS; i++) {
    itoa(i,  sensor, 10);
    Serial.println(sensor);
	  if (message.indexOf("A" + (String)sensor) > 1) send_message("MSRA" + (String)sensor, i);
  }
} 
void setup()
{
	pinMode(A0, INPUT);
	pinMode(A1, INPUT);
	pinMode(A2, INPUT);
	pinMode(A3, INPUT);
	pinMode(A4, INPUT);
	pinMode(A5, INPUT);
	Serial.begin(BAUDRATE);
	espSerial.begin(BAUDRATE);
	while (!Serial);
  while (!espSerial);
	fill_readouts();
  Serial.println("Arduino ready");
}

void loop()
{
	if (Serial.available() > 0) {
		last_message = Serial.readStringUntil('#');
		Serial.println("Debug - last message: " + last_message);
		handle_message(last_message);
		/*
        if (last_message == "get A0"){
			send_message("MSRA0", 0);
		} else if (last_message == "get A1"){
			send_message("MSRA1", 1);
		} else if (last_message == "get A2"){
			send_message("MSRA2", 2);
		} else if (last_message == "get A3"){
			send_message("MSRA3", 3);
		} else if (last_message == "get A4"){
			send_message("MSRA4", 4);
		} else if (last_message == "get A5"){
			send_message("MSRA5", 5);
		}
		*/
	} else {
    fill_readouts();
	}

}
